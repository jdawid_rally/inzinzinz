import numpy as np
import cv2
from mss import mss
from PIL import Image
import pytesseract

bounding_box = {'top': 100, 'left': 100, 'width':380 , 'height': 500}

sct = mss()
pytesseract.pytesseract.tesseract_cmd = 'D:\\Soft\\Tesseract-OCR\\tesseract.exe'
while True:
    sct_img = sct.grab(bounding_box)
    cv2.imshow('screen', np.array(sct_img))
    #im = np.array(sct_img, dtype=np.uint8)
    #im = np.flip(im[:, :, :3], 2)  # BGRA -> RGB conversion
    #text = pytesseract.image_to_float(im)
    #print(text)
    #print(pytesseract.image_to_string(sct))
    if (cv2.waitKey(1) & 0xFF) == ord('q'):
        cv2.destroyAllWindows()
        break