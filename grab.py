import numpy as np
import cv2
from mss import mss
from PIL import Image
import pytesseract
import os

def unsharp_mask(image, kernel_size=(5, 5), sigma=4.0, amount=4.0, threshold=0):
    """Return a sharpened version of the image, using an unsharp mask."""
    blurred = cv2.GaussianBlur(image, kernel_size, sigma)
    sharpened = float(amount + 1) * image - float(amount) * blurred
    sharpened = np.maximum(sharpened, np.zeros(sharpened.shape))
    sharpened = np.minimum(sharpened, 255 * np.ones(sharpened.shape))
    sharpened = sharpened.round().astype(np.uint8)
    if threshold > 0:
        low_contrast_mask = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)
    return sharpened


bounding_box = {'top': 100, 'left': 850, 'width': 100, 'height': 50}
kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
sct = mss()
pytesseract.pytesseract.tesseract_cmd = 'D:\\Soft\\Tesseract-OCR\\tesseract.exe'
while True:
    sct_img = sct.grab(bounding_box)
    base = np.array(sct_img)
    base = cv2.resize(base,None,fx=5,fy=5)
    #base = cv2.filter2D(base, -1, kernel)
    #base = unsharp_mask(base)
    #base = cv2.Canny(base,230,350)
    cv2.imshow('screen', np.array(base))
    base = np.array(sct_img, dtype=np.uint8)
    base = base
    base = np.flip(base[:, :, :3], 2)  # BGRA -> RGB conversion
    text = pytesseract.image_to_string(base)
    os.system('cls')
    print(text)
    #print(pytesseract.image_to_string(sct))
    if (cv2.waitKey(1) & 0xFF) == ord('q'):
        cv2.destroyAllWindows()
        break




