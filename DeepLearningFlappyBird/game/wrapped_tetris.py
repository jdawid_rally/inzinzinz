import numpy as np
import cv2
from mss import mss
from PIL import Image
import pytesseract
import os
from pynput.keyboard import Key, Controller
import ctypes

class GameState(object):

	def __init__(self):
		self.score = 0
		self.score_bounding_box = {'top': 115, 'left': 380, 'width': 100, 'height': 30}
		self.game_bounding_box = {'top': 100, 'left': 100, 'width':380 , 'height': 500}
		self.sct = mss()
		pytesseract.pytesseract.tesseract_cmd = 'D:\\Soft\\Tesseract-OCR\\tesseract.exe'
		self.keyboard = Controller()

	def frame_step(self, input_actions):
		self.moveBrick(input_actions)
		old_points = self.score
		new_points = self.grabPoints()
		if new_points != "":
			reward = new_points - old_points
			reward = 0.001*reward
		else:
			reward = 0

		terminal = False
		image_data = self.grabImage()

		return image_data, reward, terminal

	def grabPoints(self):
		kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
		sct_img = self.sct.grab(self.score_bounding_box)
		#cv2.imshow('screen', np.array(sct_img))
		im = np.array(sct_img, dtype=np.uint8)
		im = np.flip(im[:, :, :3], 2)  # BGRA -> RGB conversion
		base = cv2.resize(im,None,fx=10,fy=10)
		base = cv2.filter2D(im, -1, kernel)
		im = cv2.resize(im, (200,60));
		text = pytesseract.image_to_string(im, config="--psm 13")
		if text == '':
			return self.score
		else:
			try:
				return int(text)
			except:
				return self.score

	def grabImage(self):
		return np.array(self.sct.grab(self.game_bounding_box))

	def moveBrick(self, input_actions):
		keyboard = self.keyboard
		if input_actions[0] == 1:
			keyboard.press(Key.up)
			keyboard.release(Key.up)
		if input_actions[1] == 1:
			keyboard.press(Key.down)
			keyboard.release(Key.down)
		if input_actions[2] == 1:
			keyboard.press(Key.left)
			keyboard.release(Key.left)
		if input_actions[3] == 1:
			keyboard.press(Key.right)
			keyboard.release(Key.right)

#	def resize(self, cv_image, factor):
#		new_size = tuple(map(lambda x: x * factor, cv_image.shape[::-1]))
#		print(new_size)
#		return cv_image
#		#return cv2.resize(cv_image, new_size)#